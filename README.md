# Demo SICPA



## Proyecto Demo SICPA

Proyecto Demo para SICPA

### Este proyecto tiene como objetivo mostrar la funcionalidad de Sprig Boot
### Utilitarios previos
```
# Con JDK 11 minimo
# Base de datos Postgres V 11 minimo
# Intellij
# Utiliza Gitlab como repositorio
# Swagge V2
# Postman
```
## Pasos
### 1. Creacion credenciales Base de Datos
Este demo utiliza Postgres V 11
```
1. Creamos usuario sicpa con clave sicpa y con privilegios creacion DB
2. Creamos base de datos sicpadb y atamos al nuevo usuario
3. El script se encuentra alojado en la sección de recursos -> script
```
![Proyecto SICPA SWAGGER V2](https://gitlab.com/victorjcardenast/sicpabackend/-/blob/main/src/main/resources/img/swagger%20V2.jpg)
```
### 2. Clonamos el repositorio en el directorio a su eleccion con el comando
```
cd directorioproyecto
Abrimos gitbash
git init
git clone https://gitlab.com/victorjcardenast/sicpabackend
```
### 3. Ejecutamos Intellij y abrimos el proyecto
Esperamos a que MAVEN actualice el proyecto

Ejecutamos el archivo: DemoApplication.java

## Api Rest
### El proyecto contiene los servicios ncesarios para la creación, actualización de 
    Empleados, Empresas y Deparrtamentos
```
```
SWAGGER: es una aplicación que permite mostrar la estructura de servicios y documentación 
de una aplicación realizada en Spring Boot, para est5e proyecto ya se encuentra configurada
y para ejecutar SWAGGEr es necesario correr la aplicación en modo local e invocar
la URL que se muestra a continuación.
URL: http://localhost:8080/v2/api-docs 

```
## Built With

* [Gradle](https://commons.apache.org/proper/commons-configuration/dependency-info.html) - Dependency Management

## Authors

* **Víctor Cárdenas** - [victor-cardenas](https://gitlab.com/victorjcardenast)