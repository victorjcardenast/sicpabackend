package com.sicpa.demosicpa.modules.employee.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sicpa.demosicpa.modules.department.entity.Department;
import com.sicpa.demosicpa.modules.utils.entity.MasterEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "departments_employees", schema = "sicpa")
public class DepartmentEmployee extends MasterEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_department")
    @JsonIgnoreProperties(value = "departments")
    private Department department;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_employee")
    @JsonIgnoreProperties(value = "employees")
    private Employee employee;

    public DepartmentEmployee() {}

    public DepartmentEmployee(Department department, Employee employee) {
        this.department = department;
        this.employee = employee;
    }
}
