package com.sicpa.demosicpa.modules.employee.repository;

import com.sicpa.demosicpa.modules.employee.entity.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
