package com.sicpa.demosicpa.modules.employee.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sicpa.demosicpa.modules.utils.entity.MasterEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "employees", schema = "sicpa")
public class Employee extends MasterEntity {

    @Column
    private Date birthday;

    @Column
    private String email;

    @Column
    private String name;

    @Column
    private String position;

    @Column
    private String surname;

    @OneToMany(
            mappedBy = "department",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties(value = "department", allowSetters = true)
    private List<DepartmentEmployee> departments;

    @OneToMany(
            mappedBy = "employee",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties(value = "employee", allowSetters = true)
    private List<DepartmentEmployee> employees;

}
