package com.sicpa.demosicpa.modules.employee.controller;

import com.sicpa.demosicpa.modules.department.entity.Department;
import com.sicpa.demosicpa.modules.employee.dto.EmployeeDto;
import com.sicpa.demosicpa.modules.employee.service.EmployeeService;
import com.sicpa.demosicpa.modules.utils.error.ErrorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/employee",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody EmployeeDto employeeDto) {
        try{
            return ResponseEntity.status(HttpStatus.CREATED).body(employeeService.save(employeeDto));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }
    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> update(@RequestBody EmployeeDto department) {
        try{
            return ResponseEntity.status(HttpStatus.GONE).body(employeeService.update(department));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }
    }
}
