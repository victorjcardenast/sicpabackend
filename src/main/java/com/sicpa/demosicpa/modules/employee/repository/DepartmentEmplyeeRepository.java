package com.sicpa.demosicpa.modules.employee.repository;

import com.sicpa.demosicpa.modules.employee.entity.DepartmentEmployee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentEmplyeeRepository extends CrudRepository<DepartmentEmployee, Long> {

    @Query("SELECT de FROM DepartmentEmployee de " +
            "WHERE de.employee.id=:idEmployee ")
    public List<DepartmentEmployee> findByEmployeeId(@Param("idEmployee") Long idEmployee);
}
