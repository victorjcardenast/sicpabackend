package com.sicpa.demosicpa.modules.employee.service;

import com.sicpa.demosicpa.modules.department.dto.DepartmentDto;
import com.sicpa.demosicpa.modules.department.entity.Department;
import com.sicpa.demosicpa.modules.department.repository.DepartmentRepository;
import com.sicpa.demosicpa.modules.employee.dto.EmployeeDto;
import com.sicpa.demosicpa.modules.employee.entity.DepartmentEmployee;
import com.sicpa.demosicpa.modules.employee.entity.Employee;
import com.sicpa.demosicpa.modules.employee.repository.DepartmentEmplyeeRepository;
import com.sicpa.demosicpa.modules.employee.repository.EmployeeRepository;
import com.sicpa.demosicpa.modules.enterprise.entity.Enterprise;
import com.sicpa.demosicpa.modules.utils.service.GeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private DepartmentEmplyeeRepository departmentEmplyeeRepository;

    public Iterable<Employee> findAll() {

        return employeeRepository.findAll();
    }

    public Optional<Employee> findById(Long id) {
        return employeeRepository.findById(id);
    }

    public EmployeeDto save(EmployeeDto object) {
        List<DepartmentDto> departments = new ArrayList<>();
        Employee employee = new Employee();

        employee.setCreatedBy("sicpa");
        employee.setModifiedBy("sicpa");
        employee.setCreatedDate(new Date());
        employee.setModifiedDate(new Date());
        employee.setStatus(true);
        employee.setBirthday(object.getBirthday());
        employee.setEmail(object.getEmail());
        employee.setName(object.getName());
        employee.setPosition(object.getPosition());
        employee.setSurname(object.getSurname());

        employee = employeeRepository.save(employee);

        for (DepartmentDto dpto : object.getDepartments()) {
            Department dptoAux = new Department();
            dptoAux = departmentRepository.findById(dpto.getId()).get();

            DepartmentEmployee de = new DepartmentEmployee(dptoAux, employee);
            de.setCreatedBy("sicpa");
            de.setModifiedBy("sicpa");
            de.setCreatedDate(new Date());
            de.setModifiedDate(new Date());
            de.setStatus(true);
            departmentEmplyeeRepository.save(de);
            departments.add(new DepartmentDto(dptoAux.getId(), dptoAux.getName()));
        }

        EmployeeDto dto = new EmployeeDto();
        dto.setId(employee.getId());
        dto.setBirthday(employee.getBirthday());
        dto.setEmail(employee.getEmail());
        dto.setName(employee.getName());
        dto.setPosition(employee.getPosition());
        dto.setSurname(employee.getSurname());
        dto.setStatus(employee.getStatus());
        dto.setDepartments(departments);
        return dto;
    }

    public Employee update(EmployeeDto object) {

        List<Department> duplicateDpt = new ArrayList<>();
        List<Department> distinctBefore = new ArrayList<>();
        List<Long> distinctAfter = new ArrayList<>();

        Employee employee = employeeRepository.findById(object.getId()).get();
        employee.setModifiedBy("sicpa");
        employee.setModifiedDate(new Date());
        employee.setStatus(object.getStatus());

        employee.setBirthday(object.getBirthday());
        employee.setEmail(object.getEmail());
        employee.setName(object.getName());
        employee.setPosition(object.getPosition());
        employee.setSurname(object.getSurname());

//        employee = employeeRepository.save(employee);

        Iterable<Department> departmentList = departmentRepository.findAll();

        List<DepartmentEmployee> dptEmpList = departmentEmplyeeRepository.findByEmployeeId(employee.getId());

        for (DepartmentDto objDptDto : object.getDepartments()) {
            for (DepartmentEmployee objDpt : dptEmpList) {
                if(objDptDto.getId() == objDpt.getDepartment().getId()) {
                    duplicateDpt.add(objDpt.getDepartment());
                    break;
                }
            }
        }

        if(duplicateDpt.size() > 0) {
            duplicateDpt = duplicateDpt.stream().distinct().collect(Collectors.toList());
        }

        for (Department objDptDtoD : duplicateDpt) {
            for (DepartmentDto objDptDto : object.getDepartments()) {
                if(objDptDtoD.getId() != objDptDto.getId()) {
                    Long dpt = departmentRepository.findById(objDptDto.getId()).get().getId();
                    distinctAfter.add(dpt);
                    break;
                }
            }
        }

        if(!distinctAfter.isEmpty()) {
            distinctAfter = distinctAfter.stream().distinct().collect(Collectors.toList());
        }


        for (Department objDptDto : duplicateDpt) {
            for (DepartmentEmployee objDpt : dptEmpList) {
                if(objDptDto.getId() != objDpt.getDepartment().getId()) {
                    objDpt.setStatus(false);
                    departmentEmplyeeRepository.save(objDpt);
                }
            }
        }

        for (DepartmentDto dpto : object.getDepartments()) {
            Department dptoAux = new Department();
            dptoAux = departmentRepository.findById(dpto.getId()).get();

            DepartmentEmployee de = new DepartmentEmployee(dptoAux, employee);
            de.setCreatedBy("sicpa");
            de.setModifiedBy("sicpa");
            de.setCreatedDate(new Date());
            de.setModifiedDate(new Date());
            de.setStatus(true);
//            departmentEmplyeeRepository.save(de);
//            departments.add(new DepartmentDto(dptoAux.getId(), dptoAux.getName()));
        }

        return null;
    }
}
