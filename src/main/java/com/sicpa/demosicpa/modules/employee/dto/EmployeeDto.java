package com.sicpa.demosicpa.modules.employee.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sicpa.demosicpa.modules.department.dto.DepartmentDto;
import com.sicpa.demosicpa.modules.department.entity.Department;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class EmployeeDto {

    private Long id;

    private Date birthday;

    private String email;

    private String name;

    private String position;

    private String surname;

    private Boolean status;

    private List<DepartmentDto> departments;

    public EmployeeDto() {}

    public EmployeeDto(
            Long id,
            Date birthday,
            String email,
            String name,
            String position,
            String surname,
            Boolean status,
            List<DepartmentDto> departments) {
        this.id = id;
        this.birthday = birthday;
        this.email = email;
        this.name = name;
        this.position = position;
        this.surname = surname;
        this.status = status;
        this.departments = departments;
    }
}
