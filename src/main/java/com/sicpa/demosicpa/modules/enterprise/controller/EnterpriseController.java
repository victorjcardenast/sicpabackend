package com.sicpa.demosicpa.modules.enterprise.controller;

import com.sicpa.demosicpa.modules.enterprise.entity.Enterprise;
import com.sicpa.demosicpa.modules.enterprise.service.EnterpriseService;
import com.sicpa.demosicpa.modules.utils.error.ErrorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/enterprise",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class EnterpriseController {

    @Autowired
    private EnterpriseService enterpriseService;

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody Enterprise enterprise) {

        try{
            return ResponseEntity.status(HttpStatus.CREATED).body(enterpriseService.save(enterprise));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }

    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> update(@RequestBody Enterprise enterprise) {

        try{
            return ResponseEntity.status(HttpStatus.GONE).body(enterpriseService.update(enterprise));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }

    }

    @GetMapping
    public ResponseEntity<?> getAll() {

        try{
            return ResponseEntity.status(HttpStatus.OK).body(enterpriseService.findAll());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }

    }
}
