package com.sicpa.demosicpa.modules.enterprise.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sicpa.demosicpa.modules.department.entity.Department;
import com.sicpa.demosicpa.modules.utils.entity.MasterEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "enterprises", schema = "sicpa")
public class Enterprise extends MasterEntity implements Serializable {

    @Column(nullable = false, length = 150)
    private String address;

    @Column(nullable = false, length = 80)
    private String name;

    @Column(length = 20)
    private String phone;

    @OneToMany(
            mappedBy = "enterprise",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnoreProperties(value = "enterprise", allowSetters = true)
    private List<Department> departments;

}
