package com.sicpa.demosicpa.modules.enterprise.repository;

import com.sicpa.demosicpa.modules.enterprise.entity.Enterprise;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnterpriseRepository extends CrudRepository<Enterprise, Long> {
}
