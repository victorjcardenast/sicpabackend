package com.sicpa.demosicpa.modules.enterprise.service;

import com.sicpa.demosicpa.modules.enterprise.entity.Enterprise;
import com.sicpa.demosicpa.modules.enterprise.repository.EnterpriseRepository;
import com.sicpa.demosicpa.modules.utils.service.GeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class EnterpriseService implements GeneralService<Enterprise> {

    @Autowired
    private EnterpriseRepository enterpriseRepository;

    @Override
    public Iterable<Enterprise> findAll() {
        return enterpriseRepository.findAll();
    }

    @Override
    public Optional<Enterprise> findById(Long id) {
        return enterpriseRepository.findById(id);
    }

    @Override
    public Enterprise save(Enterprise object) {
        object.setCreatedBy("sicpa");
        object.setModifiedBy("sicpa");
        object.setCreatedDate(new Date());
        object.setModifiedDate(new Date());
        object.setStatus(true);

        return enterpriseRepository.save(object);
    }

    @Override
    public Enterprise update(Enterprise object) {

        Enterprise enterprise = enterpriseRepository.findById(object.getId()).get();
        enterprise.setModifiedBy("sicpa");
        enterprise.setModifiedDate(new Date());
        enterprise.setStatus(object.getStatus());

        enterprise.setAddress(object.getAddress());
        enterprise.setName(object.getName());
        enterprise.setPhone(object.getPhone());

        return enterpriseRepository.save(enterprise);
    }
}
