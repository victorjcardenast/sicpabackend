package com.sicpa.demosicpa.modules.department.service;

import com.sicpa.demosicpa.modules.department.entity.Department;
import com.sicpa.demosicpa.modules.department.repository.DepartmentRepository;
import com.sicpa.demosicpa.modules.enterprise.entity.Enterprise;
import com.sicpa.demosicpa.modules.enterprise.repository.EnterpriseRepository;
import com.sicpa.demosicpa.modules.utils.service.GeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class DepartmentService implements GeneralService<Department> {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EnterpriseRepository enterpriseRepository;

    @Override
    public Iterable<Department> findAll() {
        return departmentRepository.findAll();
    }

    @Override
    public Optional<Department> findById(Long id) {
        return departmentRepository.findById(id);
    }

    @Override
    public Department save(Department object) {
        object.setCreatedBy("sicpa");
        object.setModifiedBy("sicpa");
        object.setCreatedDate(new Date());
        object.setModifiedDate(new Date());
        object.setStatus(true);
        object.setEnterprise(enterpriseRepository.findById(object
                .getEnterprise().getId()).get());

        return departmentRepository.save(object);
    }

    @Override
    public Department update(Department object) {
        Department department = new Department();
        Optional<Enterprise> enterprise = enterpriseRepository.findById(object.getEnterprise().getId());

        department = departmentRepository.findById(object.getId()).get();
        department.setModifiedBy("sicpa");
        department.setModifiedDate(new Date());
        department.setStatus(object.getStatus());

        department.setDescription(object.getDescription());
        department.setName(object.getName());
        department.setPhone(object.getPhone());
        department.setEnterprise(enterprise.get());


        return departmentRepository.save(department);
    }

    public Department findByame(String name) {
        Optional<Department> dpt = departmentRepository.findByName(name);
        Department dptAux =  new Department();
        dptAux = dpt.get();
1        return dptAux;
    }
}
