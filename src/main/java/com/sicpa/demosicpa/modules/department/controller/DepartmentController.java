package com.sicpa.demosicpa.modules.department.controller;

import com.sicpa.demosicpa.modules.department.entity.Department;
import com.sicpa.demosicpa.modules.department.service.DepartmentService;
import com.sicpa.demosicpa.modules.utils.error.ErrorDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "api/department",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody Department department) {

        try{
            return ResponseEntity.status(HttpStatus.CREATED).body(departmentService.save(department));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }

    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> update(@RequestBody Department department) {
        try{
            return ResponseEntity.status(HttpStatus.GONE).body(departmentService.update(department));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }
    }

    @GetMapping
    public ResponseEntity<?> getAll() {
        try{
            return ResponseEntity.status(HttpStatus.OK).body(departmentService.findAll());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }
    }

    @GetMapping(value = "/getByName/{name}")
    public ResponseEntity<?> getByName(@PathParam("name") String name) {
        try {
            return  ResponseEntity.status(HttpStatus.OK).body(departmentService.findByame(name));

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new ErrorDto("Key account_number already exists.",
                            "SQLState: 23505", "failed"));
        }
    }
}
