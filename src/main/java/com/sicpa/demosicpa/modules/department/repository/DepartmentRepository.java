package com.sicpa.demosicpa.modules.department.repository;

import com.sicpa.demosicpa.modules.department.entity.Department;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {
    @Query("FROM Department d WHERE d.name=:name")
    public Optional<Department> findByName(@Param("name") String names);

}
