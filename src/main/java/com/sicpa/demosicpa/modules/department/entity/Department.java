package com.sicpa.demosicpa.modules.department.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sicpa.demosicpa.modules.enterprise.entity.Enterprise;
import com.sicpa.demosicpa.modules.utils.entity.MasterEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "departments", schema = "sicpa")
public class Department extends MasterEntity implements Serializable {

    @Column(length = 500)
    private String description;

    @Column(nullable = false, length = 80)
    private String name;

    @Column(length = 20)
    private String phone;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_enterprise")
    @JsonIgnoreProperties(value = "departments")
    private Enterprise enterprise;

    public Department() {
    }

    public Department(String description, String name, String phone) {
        this.description = description;
        this.name = name;
        this.phone = phone;
    }
}
