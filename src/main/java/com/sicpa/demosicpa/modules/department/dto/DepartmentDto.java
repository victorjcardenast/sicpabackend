package com.sicpa.demosicpa.modules.department.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DepartmentDto {

    private Long id;

    private String name;

    public DepartmentDto() {}

    public DepartmentDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
