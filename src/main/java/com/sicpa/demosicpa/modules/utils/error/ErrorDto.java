package com.sicpa.demosicpa.modules.utils.error;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDto {
    private String message;
    private String code;
    private String state;

    public ErrorDto() {
    }

    public ErrorDto(String message, String code, String state) {
        this.message = message;
        this.code = code;
        this.state = state;
    }
}
