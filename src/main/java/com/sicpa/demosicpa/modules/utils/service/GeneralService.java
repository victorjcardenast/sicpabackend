package com.sicpa.demosicpa.modules.utils.service;

import java.util.Optional;

public interface GeneralService<T> {

    public Iterable<T> findAll();

    public Optional<T> findById(Long id);

    public T save(T object);

    public T update(T object);
}
