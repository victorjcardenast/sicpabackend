package com.sicpa.demosicpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemosicpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemosicpaApplication.class, args);
	}

}
